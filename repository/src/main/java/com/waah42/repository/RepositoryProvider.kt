package com.waah42.repository

import com.waah42.repository.di.DaggerRepositoryComponent
import com.waah42.repository.di.RepositoryComponent
import com.waah42.repository.btc.BTCRepository
import javax.inject.Inject


class RepositoryProvider {
    private var component: RepositoryComponent? = null

    @Inject
    lateinit var btcRepository: BTCRepository

    init {
        component = DaggerRepositoryComponent
            .builder()
            .build()

        component?.inject(this)
    }
}