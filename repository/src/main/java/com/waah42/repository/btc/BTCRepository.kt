package com.waah42.repository.btc

import android.arch.lifecycle.LiveData
import com.waah42.repository.response.AllTimePrice
import com.waah42.repository.response.DailyPrice
import com.waah42.repository.response.MonthlyPrice


interface BTCRepository {

    fun getDailyPrice(onError: (msg: String) -> Unit): LiveData<List<DailyPrice>>

    fun getMonthlyPrice(onError: (msg: String) -> Unit): LiveData<List<MonthlyPrice>>

    fun getAllTimePrice(onError: (msg: String) -> Unit): LiveData<List<AllTimePrice>>

}