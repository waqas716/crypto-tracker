package com.waah42.repository.btc

import com.waah42.repository.response.AllTimePrice
import com.waah42.repository.response.DailyPrice
import com.waah42.repository.response.MonthlyPrice
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

internal interface BTCService {
    @GET("/indices/global/history/BTCEUR")
    fun getDailyPrice(@Query("period") period: String = "daily", @Query("format") format: String = "json"): Call<List<DailyPrice>>

    @GET("/indices/global/history/BTCEUR")
    fun getMonthlyPrice(@Query("period") period: String = "monthly", @Query("format") format: String = "json"): Call<List<MonthlyPrice>>

    @GET("/indices/global/history/BTCEUR")
    fun getAllTimePrice(@Query("period") period: String = "alltime", @Query("format") format: String = "json"): Call<List<AllTimePrice>>


}