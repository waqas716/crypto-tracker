package com.waah42.repository.btc

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.waah42.repository.response.AllTimePrice
import com.waah42.repository.response.DailyPrice
import com.waah42.repository.response.MonthlyPrice
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject


internal class BTCRepositoryImpl @Inject constructor(private var BTCService: BTCService) :
    BTCRepository {

    override fun getDailyPrice(onError: (msg: String) -> Unit): LiveData<List<DailyPrice>> {

        val priceData: MutableLiveData<List<DailyPrice>> = MutableLiveData()
        val request = BTCService.getDailyPrice()

        request.enqueue(object : Callback<List<DailyPrice>> {
            override fun onFailure(call: Call<List<DailyPrice>>, t: Throwable?) {
                Timber.e(t)
                onError("Network request failed with: ${t?.message}")
            }

            override fun onResponse(call: Call<List<DailyPrice>>,
                                    response: Response<List<DailyPrice>>) {
                when (response.code()) {
                    200 -> {
                        priceData.value = response.body()
                    }
                    else -> onError(response.message())
                }
            }

        })

        return priceData
    }

    override fun getMonthlyPrice(onError: (msg: String) -> Unit): LiveData<List<MonthlyPrice>> {

        val priceData: MutableLiveData<List<MonthlyPrice>> = MutableLiveData()
        val request = BTCService.getMonthlyPrice()

        request.enqueue(object : Callback<List<MonthlyPrice>> {
            override fun onFailure(call: Call<List<MonthlyPrice>>, t: Throwable?) {
                Timber.e(t)
                onError("Network request failed with: ${t?.message}")
            }

            override fun onResponse(call: Call<List<MonthlyPrice>>,
                                    response: Response<List<MonthlyPrice>>) {
                when (response.code()) {
                    200 -> {
                        priceData.value = response.body()
                    }
                    else -> onError(response.message())
                }
            }

        })

        return priceData
    }

    override fun getAllTimePrice(onError: (msg: String) -> Unit): LiveData<List<AllTimePrice>> {
        val priceData: MutableLiveData<List<AllTimePrice>> = MutableLiveData()
        val request = BTCService.getAllTimePrice()

        request.enqueue(object : Callback<List<AllTimePrice>> {
            override fun onFailure(call: Call<List<AllTimePrice>>, t: Throwable?) {
                Timber.e(t)
                onError("Network request failed with: ${t?.message}")
            }

            override fun onResponse(call: Call<List<AllTimePrice>>,
                                    response: Response<List<AllTimePrice>>) {
                when (response.code()) {
                    200 -> {
                        priceData.value = response.body()
                    }
                    else -> {
                        Timber.e(response.message())
                        onError(response.message())
                    }
                }
            }
        })

        return priceData
    }

}