package com.waah42.repository.response

data class DailyPrice(var time: String, var average: Double)