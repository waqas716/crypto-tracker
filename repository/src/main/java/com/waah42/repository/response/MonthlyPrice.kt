package com.waah42.repository.response


data class MonthlyPrice(val average: Double,
                        val high: Double,
                        val low: Double,
                        val time: String,
                        val open: Double)