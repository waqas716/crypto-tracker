package com.waah42.repository.response


data class AllTimePrice(val average: Double,
                        val high: Double,
                        val low: Double,
                        val time: String,
                        val open: String,
                        val volume: String)