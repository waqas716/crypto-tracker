package com.waah42.repository.di

import com.waah42.repository.btc.BTCRepository
import com.waah42.repository.btc.BTCRepositoryImpl
import com.waah42.repository.btc.BTCService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
internal class RepositoryModule {

    @Provides
    @Singleton
    internal fun providesBTCService(): BTCService {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://apiv2.bitcoinaverage.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(BTCService::class.java)
    }

    @Provides
    @Singleton
    internal fun provideBTCService(btcRepositoryImpl: BTCRepositoryImpl): BTCRepository {
        return btcRepositoryImpl
    }

}