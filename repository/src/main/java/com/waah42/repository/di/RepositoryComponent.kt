package com.waah42.repository.di

import com.waah42.repository.RepositoryProvider
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(RepositoryModule::class))
interface RepositoryComponent {

    @Component.Builder
    interface Builder {

        fun build(): RepositoryComponent
    }

    fun inject(repositoryProvider: RepositoryProvider)

}