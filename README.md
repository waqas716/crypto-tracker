# Crypto Tracker
Simple crypto tracker app to showcase modern android architecture and use of [bitcoin-cpverage](https://bitcoinaverage.com/api) api.

## Architecture
The sample app uses android architecture components to separate UI completely from logic using LiveData and ViewModel approach. The repository module hides the implementation detail of how prices are fetched, right now it’s fetched from remote service but in future we will add a local cache using Room to support offline view of already cached data. The final architecture will look like this ![architecture](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)


### Libraries used in this sample app:
* [Kotlin](https://kotlinlang.org)
* [Android architecture components (ViewModels, LiveData)](https://developer.android.com/topic/libraries/architecture/guide.html)
* [Spark](https://github.com/robinhood/spark) to show prices in a sparkline chart
* [Retrofit](https://square.github.io/retrofit/) 
* [Dagger2](https://google.github.io/dagger/)
* [Timber](https://github.com/JakeWharton/timber)

### Next steps:
* Use developer permission required [data-since-timestamp](https://apiv2.bitcoinaverage.com/#data-since-timestamp) endpoint to get data in time ranges like 1 hour, 1 week, 1 month, 1 year. This will allow to have better visualization of chart and also less data to download so it will reduce download time.
* Use [price-at-timestamp](https://apiv2.bitcoinaverage.com/#price-at-timestamp) endpoint to get value at a specific timestamp
* Finalise UI for daily,monthly and all time charts, if using same chart type for multiple views then code should be refactored to re-use adapters, endpoints and fragments.
* Show legend (time range, peek price value) around sparkline chart view
* Better color and UI for price values, show daily, monthly changes like +15% etc with price.
* Show error view and give a retry button in case of timeouts and network errors
* Use custom bottom navigation icons
* Add some Unit and UI tests
