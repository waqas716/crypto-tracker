package com.waah42.cryptotracker.ui.adapter

import com.robinhood.spark.SparkAdapter
import com.waah42.repository.response.DailyPrice


class DailySparkAdapter(private val data: MutableList<DailyPrice>) : SparkAdapter() {

    override fun getY(index: Int): Float = data[index].average.toFloat()


    override fun getItem(index: Int): Any = data[index]

    override fun getCount(): Int = data.size

    override fun hasBaseLine(): Boolean = true

    override fun getBaseLine(): Float {
        return data.map { it.average.toFloat() }.average().toFloat()
    }

    fun updateData(newData: List<DailyPrice>?) {
        newData?.let {
            data.clear()
            data.addAll(newData.reversed())
            notifyDataSetChanged()
        }
    }
}