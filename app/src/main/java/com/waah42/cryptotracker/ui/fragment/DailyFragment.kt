package com.waah42.cryptotracker.ui.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.robinhood.spark.SparkView
import com.waah42.cryptotracker.R
import com.waah42.cryptotracker.ui.adapter.DailySparkAdapter
import com.waah42.cryptotracker.vm.DailyViewModel
import com.waah42.repository.response.DailyPrice
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_daily.*
import javax.inject.Inject


class DailyFragment : Fragment() {

    private val dailySparkAdapter = DailySparkAdapter(arrayListOf())

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var dailyViewModel: DailyViewModel

    private var recentPrice: String = ""
    private var recentTime: String = ""

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_daily, container, false)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            dailySparkView.lineColor = ContextCompat.getColor(it, R.color.sparkLineColor)
            dailySparkView.fillType = SparkView.FillType.DOWN
        }

        dailyViewModel =
                ViewModelProviders.of(this, viewModelFactory).get(DailyViewModel::class.java)


        dailySparkView.adapter = dailySparkAdapter

        dailyViewModel.observableDailyPrice.observe(this,
            Observer {
                dailyProgressBar.visibility = View.GONE
                it?.get(0)?.let { dailyPrice -> updateCurrentPrice(dailyPrice) }
                dailySparkAdapter.updateData(it)
            })


        dailySparkView.setScrubListener { item ->
            item?.let {
                val dailyPrice = it as DailyPrice
                dailyScrubValue.text = dailyPrice.average.toString()
                dailyDateView.text = dailyPrice.time
            } ?: setCurrentPriceAfterScrub()

        }

        dailyViewModel.observableOnError.observe(this,
            Observer<String> { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() })
    }

    private fun updateCurrentPrice(dailyPrice: DailyPrice) {
        recentPrice = dailyPrice.average.toString()
        recentTime = dailyPrice.time

        dailyScrubValue.text = recentPrice
        dailyDateView.text = recentTime
    }

    private fun setCurrentPriceAfterScrub() {
        dailyScrubValue.text = recentPrice
        dailyDateView.text = recentTime
    }
}