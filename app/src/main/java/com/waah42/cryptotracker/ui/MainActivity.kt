package com.waah42.cryptotracker.ui

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.waah42.cryptotracker.R
import com.waah42.cryptotracker.ui.fragment.AllTimeFragment
import com.waah42.cryptotracker.ui.fragment.DailyFragment
import com.waah42.cryptotracker.ui.fragment.MonthlyFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var dailyFragment: DailyFragment

    @Inject
    lateinit var monthlyFragment: MonthlyFragment

    @Inject
    lateinit var allTimeFragment: AllTimeFragment

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    showDailyFragment()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {
                    showMonthlyFragment()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_notifications -> {
                    showAllTimeFragment()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        supportFragmentManager.beginTransaction().replace(R.id.frag_container, dailyFragment)
            .commit()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    private fun showDailyFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.frag_container, dailyFragment)
            .commit()
    }

    private fun showMonthlyFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.frag_container, monthlyFragment)
            .commit()
    }

    private fun showAllTimeFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.frag_container, allTimeFragment)
            .commit()
    }
}
