package com.waah42.cryptotracker.ui.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.robinhood.spark.SparkView
import com.waah42.cryptotracker.R
import com.waah42.cryptotracker.ui.adapter.AllTimeSparkAdapter
import com.waah42.cryptotracker.vm.AllTimeViewModel
import com.waah42.repository.response.AllTimePrice
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_alltime.*
import javax.inject.Inject


class AllTimeFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var allTimeViewModel: AllTimeViewModel
    private val allTimeSparkAdapter =
        AllTimeSparkAdapter(arrayListOf())

    private var recentPrice: String = ""
    private var recentTime: String = ""

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_alltime, container, false)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            allTimeSparkView.lineColor = ContextCompat.getColor(it, R.color.sparkLineColor)
            allTimeSparkView.fillType = SparkView.FillType.DOWN
        }

        allTimeViewModel =
                ViewModelProviders.of(this, viewModelFactory).get(AllTimeViewModel::class.java)
        allTimeSparkView.adapter = allTimeSparkAdapter

        allTimeViewModel.observableAllTimePrice.observe(this,
            Observer {
                allTimeProgressBar.visibility = View.GONE
                it?.get(0)?.let { allTimePrice -> updateCurrentPrice(allTimePrice) }
                allTimeSparkAdapter.updateData(it)
            })


        allTimeSparkView.setScrubListener { item ->
            item?.let {
                val allTimePrice = it as AllTimePrice
                allTimeScrubValue.text = allTimePrice.average.toString()
                allTimeDateView.text = allTimePrice.time
            } ?: setCurrentPriceAfterScrub()
        }

        allTimeViewModel.observableOnError.observe(this,
            Observer<String> { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() })
    }

    private fun updateCurrentPrice(allTimePrice: AllTimePrice) {
        recentPrice = allTimePrice.average.toString()
        recentTime = allTimePrice.time

        allTimeScrubValue.text = recentPrice
        allTimeDateView.text = recentTime
    }

    private fun setCurrentPriceAfterScrub() {
        allTimeScrubValue.text = recentPrice
        allTimeDateView.text = recentTime
    }
}