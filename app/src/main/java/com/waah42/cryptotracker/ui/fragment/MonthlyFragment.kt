package com.waah42.cryptotracker.ui.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.robinhood.spark.SparkView
import com.waah42.cryptotracker.R
import com.waah42.cryptotracker.ui.adapter.MonthlySparkAdapter
import com.waah42.cryptotracker.vm.MonthlyViewModel
import com.waah42.repository.response.MonthlyPrice
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_monthly.*
import javax.inject.Inject


class MonthlyFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var monthlyViewModel: MonthlyViewModel
    private val monthlySparkAdapter =
        MonthlySparkAdapter(arrayListOf())

    private var recentPrice: String = ""
    private var recentTime: String = ""

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_monthly, container, false)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            monthlySparkView.lineColor = ContextCompat.getColor(it, R.color.sparkLineColor)
            monthlySparkView.fillType = SparkView.FillType.DOWN
        }

        monthlyViewModel =
                ViewModelProviders.of(this, viewModelFactory).get(MonthlyViewModel::class.java)
        monthlySparkView.adapter = monthlySparkAdapter

        monthlyViewModel.observableMonthlyPrice.observe(this,
            Observer {
                monthlyProgressBar.visibility = View.GONE
                it?.get(0)?.let { monthlyPrice -> updateCurrentPrice(monthlyPrice) }
                monthlySparkAdapter.updateData(it)
            })


        monthlySparkView.setScrubListener { item ->
            item?.let {
                val monthlyPrice = it as MonthlyPrice
                monthlyScrubValue.text = monthlyPrice.average.toString()
                monthlyDateView.text = monthlyPrice.time
            } ?: setCurrentPriceAfterScrub()
        }

        monthlyViewModel.observableOnError.observe(this,
            Observer<String> { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() })
    }

    private fun updateCurrentPrice(monthlyPrice: MonthlyPrice) {
        recentPrice = monthlyPrice.average.toString()
        recentTime = monthlyPrice.time

        monthlyScrubValue.text = recentPrice
        monthlyDateView.text = recentTime
    }

    private fun setCurrentPriceAfterScrub() {
        monthlyScrubValue.text = recentPrice
        monthlyDateView.text = recentTime
    }
}