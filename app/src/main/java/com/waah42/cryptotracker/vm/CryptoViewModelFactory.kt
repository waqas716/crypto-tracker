package com.waah42.cryptotracker.vm

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.waah42.repository.btc.BTCRepository
import javax.inject.Inject


class CryptoViewModelFactory @Inject constructor(private val repository: BTCRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DailyViewModel::class.java)) {
            return DailyViewModel(repository) as T
        } else if (modelClass.isAssignableFrom(MonthlyViewModel::class.java)) {
            return MonthlyViewModel(repository) as T
        } else if (modelClass.isAssignableFrom(AllTimeViewModel::class.java)) {
            return AllTimeViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}