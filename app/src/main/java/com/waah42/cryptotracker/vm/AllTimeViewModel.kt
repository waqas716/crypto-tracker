package com.waah42.cryptotracker.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.waah42.repository.btc.BTCRepository
import com.waah42.repository.response.AllTimePrice
import javax.inject.Inject


class AllTimeViewModel @Inject constructor(repository: BTCRepository) : ViewModel() {
    val observableAllTimePrice: LiveData<List<AllTimePrice>>

    val observableOnError: MutableLiveData<String> = MutableLiveData()

    init {
        observableAllTimePrice = repository.getAllTimePrice(::onError)
    }

    private fun onError(message: String) {
        observableOnError.value = message
    }
}