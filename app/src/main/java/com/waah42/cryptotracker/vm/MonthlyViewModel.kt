package com.waah42.cryptotracker.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.waah42.repository.btc.BTCRepository
import com.waah42.repository.response.MonthlyPrice
import javax.inject.Inject


class MonthlyViewModel @Inject constructor(repository: BTCRepository) : ViewModel() {
    val observableMonthlyPrice: LiveData<List<MonthlyPrice>>

    val observableOnError: MutableLiveData<String> = MutableLiveData()

    init {
        observableMonthlyPrice = repository.getMonthlyPrice(::onError)
    }

    private fun onError(message: String) {
        observableOnError.value = message
    }
}