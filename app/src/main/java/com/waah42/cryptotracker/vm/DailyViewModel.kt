package com.waah42.cryptotracker.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.waah42.repository.btc.BTCRepository
import com.waah42.repository.response.DailyPrice
import javax.inject.Inject


class DailyViewModel @Inject constructor(repository: BTCRepository) : ViewModel() {
    val observableDailyPrice: LiveData<List<DailyPrice>>

    val observableOnError: MutableLiveData<String> = MutableLiveData()

    init {
        observableDailyPrice = repository.getDailyPrice(::onError)
    }

    private fun onError(message: String) {
        observableOnError.value = message
    }
}