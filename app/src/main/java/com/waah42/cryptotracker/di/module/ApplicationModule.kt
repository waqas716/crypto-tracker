package com.waah42.cryptotracker.di.module

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.waah42.cryptotracker.di.scope.PerApplication
import com.waah42.cryptotracker.vm.CryptoViewModelFactory
import com.waah42.repository.RepositoryProvider
import com.waah42.repository.btc.BTCRepository
import dagger.Module
import dagger.Provides

@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    internal fun provideBTCRepository(): BTCRepository {
        return RepositoryProvider().btcRepository
    }

    @Provides
    @PerApplication
    internal fun provideViewModelFactory(cryptoViewModelFactory: CryptoViewModelFactory): ViewModelProvider.Factory {
        return cryptoViewModelFactory
    }
}