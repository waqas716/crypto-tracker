package com.waah42.cryptotracker.di

import android.app.Application
import com.waah42.cryptotracker.CryptoTrackerApp
import com.waah42.cryptotracker.di.module.ApplicationModule
import com.waah42.cryptotracker.di.module.BindingModule
import com.waah42.cryptotracker.di.scope.PerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule


@PerApplication
@Component(modules = arrayOf(BindingModule::class, ApplicationModule::class,
    AndroidSupportInjectionModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: CryptoTrackerApp)
}