package com.waah42.cryptotracker.di.module

import com.waah42.cryptotracker.di.scope.PerActivity
import com.waah42.cryptotracker.ui.fragment.AllTimeFragment
import com.waah42.cryptotracker.ui.fragment.DailyFragment
import com.waah42.cryptotracker.ui.MainActivity
import com.waah42.cryptotracker.ui.fragment.MonthlyFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BindingModule {
    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    @PerActivity
    abstract fun bindDailyFragment(): DailyFragment

    @ContributesAndroidInjector
    @PerActivity
    abstract fun bindMonthlyFragment(): MonthlyFragment

    @ContributesAndroidInjector
    @PerActivity
    abstract fun bindAllTimeFragment(): AllTimeFragment
}