package com.waah42.cryptotracker.di.module

import com.waah42.cryptotracker.ui.fragment.AllTimeFragment
import com.waah42.cryptotracker.ui.fragment.DailyFragment
import com.waah42.cryptotracker.ui.fragment.MonthlyFragment
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {
    @Provides
    fun provideDailyFragment(): DailyFragment {
        return DailyFragment()
    }

    @Provides
    fun provideMonthlyFragment(): MonthlyFragment {
        return MonthlyFragment()
    }

    @Provides
    fun provideAllTimeFragment(): AllTimeFragment {
        return AllTimeFragment()
    }

}